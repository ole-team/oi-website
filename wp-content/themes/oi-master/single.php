<?php get_header(); ?>
<section>
	<div class="container">
	<?php while ( have_posts() ) : the_post(); ?>
	<div class="row wrap-info-single">
		<div class="col m4 s12 info-single">
		<div class="row">
			<?php 
				$category = get_the_category();
				$firstCategory = $category[0]->cat_name;
				echo '<strong class="cat-single">'.$firstCategory.'</strong>';
			?>
			<div class="box-title">
				<?php 
				the_title();
				?>
			</div>
		</div>
		</div>
		<div class="col m8 s12 pic-single">
			<div class="row">
		<?php 
			if ( has_post_thumbnail() ) { 
				the_post_thumbnail( 'full' ); 
			}
		?>	</div>
		</div>
	</div>
	<div class="row">
		<div class="col m6 s12">
			<?php 
			$kpi = get_post_meta( get_the_ID(),'campaign_kpi',true );
			if(!empty($kpi)){ ?>
			<div class="kpi-single">
				<h3>Campaign KPI</h3>
				<p><?php echo $kpi; ?></p>
			</div>
			<?php } ?>
		</div>
		<div class="col m6 s12">
			<?php the_content(); ?>
		</div>
	</div>
	<div class="row box-results">
		<?php 
		$views = get_post_meta( get_the_ID(),'views',true );
		$reached = get_post_meta( get_the_ID(),'reached_users',true );
		$result = get_post_meta( get_the_ID(),'result',true );
		if(!empty($views)){
		?>
		<div class="col m2 s12 offset-m1">
			<h3 class="tt-results">RESULTS</h3>
		</div>
		<div class="col m3 s12">
			<p><strong><?php echo $views; ?></strong></p>
			<p>VIEWS</p>
		</div>
		<?php if(!empty($reached)){ ?>
		<div class="col m3 s12">
			<p><strong><?php echo $reached;?></strong></p>
			<p>REACHED USERS</p>
		</div>
		<?php } ?>
		<?php if(!empty($result)){ ?>
		<div class="col m3 s12">
			<p><strong><?php echo $result;?></strong></p>
			<p><span>ABOVE</span> THE EXPECTED RESULT</p>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
	<?php endwhile; ?>
</div>
</section>
<?php get_footer(); ?>