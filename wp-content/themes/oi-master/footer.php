<?php wp_footer(); ?>
<footer class="page-footer">
	<?php if(!is_page('contacto')){ ?> 
	<div class="container call-contact">
	<div class="row">
		<div class="col s6 offset-s3 center">
			<a href="<?php echo esc_url( get_page_link( 14 ) ); ?>" class="link-lets-talk">
				Let's talk
			</a>
		</div>
	</div>
	</div>
	<?php } ?>
	<div class="footer-copyright">
		<div class="container foot-wrap">
			<?php wp_nav_menu( array( 'theme_location' => 'footermenu', 'container'=>false) ); ?>
			<div class="copyright">
				<p>© 2022 Ole Interactive. All Rights Reserved.</p>
			</div>
		</div>
	</div>
<script type="text/javascript">
(function($) {
$(document).ready(function(){
	$(window).width(function(){
		if ($(window).width() < 720){
			$('.anima-oi').removeClass('wow');
		}
	});
	var logoNav = '<li class="logo-nav"><img src="<?php echo get_template_directory_uri();?>/assets/oi_logo_nav.png" /></li>';
	var closeNav = '<a class="sidenav-close" href="#!"> <i class="material-icons">close</i></a>';
	$('#nav-mobile').prepend(closeNav+logoNav);
	new WOW().init();
	$(window).scroll(function(){
		var scroll = $(window).scrollTop();
	  	if (scroll > 100) {
	  		$('nav').addClass('nav-light');
	  	}else{
	  		$('nav').removeClass('nav-light');
	  	}
	});
	$('.sidenav').sidenav();
	<?php if(is_front_page()){ ?>
	$('.tabs').tabs();
	<?php } ?>
});
})(jQuery);
</script>
</footer>
</body>
</html>