<?php
get_header();

?>
<section class="intro">
	<div class="container">
		<div class="content">
<?php if ( have_posts() ) : ?>
	<?php $this_category = get_category($cat); ?>
	<div class="row list-cat">
		<div class="col s12">
		<h1><?php echo $this_category->cat_name ?></h1>
		</div>
	<?php 
	$varcc = 1;
	$varstyle = '';
	$varboxcss = 'm4 offset-m8 featured-box';
	while (have_posts()) : the_post(); 
		if($varcc > 1){
			$varstyle = 'm6';
			$varboxcss = 'm7 offset-m5';
		}
	?>
		<div class="col s12 <?php echo $varstyle; ?> item-cat-med">
		<?php 
		if ( has_post_thumbnail() ) { 
			if($varcc > 1){
		    	the_post_thumbnail( 'list-pic-oi' ); 
			}else{
				the_post_thumbnail( 'full' ); 
			}
		}
		?>
		<div class="col s10 offset-s2 <?php echo $varboxcss; ?> box-info-p">
			<?php
			echo '<a href="'.get_permalink().'"><h2>'.get_the_title().'</h2><strong>+INFO</strong></a>'; 
			?>
			</div>
		</div>
	<?php 
	$varcc++;
	endwhile; ?>
	</div>
	
<?php endif; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
