<?php 
/* 
Template Name: Contact Us
*/?>
<?php get_header(); ?>
<section class="content-contact">
	<div class="container">
		<div class="row">
			<div class="col m6 offset-m3 s10 offset-s1 ">
				<ul class="items-services-contact">
					<li><img src="<?php echo get_template_directory_uri();?>/assets/oi_voice_white.png"  /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/assets/oi_noise_white.png"  /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/assets/oi_droids_white.png"  /></li>
				</ul>
				<ul class="text-services-contact">
					<li><span>1. BRANDED CONTENT</span></li>
					<li><span>2. CONTENT STUDIOS</span></li>
					<li><span>3. SOCIAL MANAGEMENT</span></li>
					<li><span>4. MEDIA BUYING</span></li>
					<li><span>5. DATA & INSIGHTS</span></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="bottom-contact">
	<div class="container">
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="row">
				<div class="col m8 offset-m2 s10 offset-s1 box-form">
					<div class="col m6 s12 pic-contact-form">
						<h2>Let's talk</h2>
						<img src="<?php echo get_template_directory_uri();?>/assets/img_contacto.png"  />
					</div>
					<div class="col m6 s12">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</section>
<?php get_footer(); ?>