<?php
/*
Template Name: Category Brands 
*/
get_header();

?>
<section class="cat-brands">
	<div class="container">
		<?php if ( have_posts() ) : ?>
		<div class="row">
			<div class="col m4 s12 pic-cat-brand">
				<img src="<?php echo get_template_directory_uri();?>/assets/ico_cs.png"  /></li>
			</div>
			<div class="col m8 s12 contenido-brand">
				<h1>We learned how to do it for our own brands:</h1>
				<div class="wrap-brand">
				<?php while (have_posts()) : the_post(); ?>
					<?php $slug = get_post_field( 'post_name', get_the_ID() ); ?>
				<div class="item-brand <?php echo $slug; ?>">
				<?php if ( has_post_thumbnail() ) { 
					the_post_thumbnail( 'full' ); 
				} ?>
				</div>
				<?php endwhile; ?>
				</div>
				<h2>before doing it for yours ;)</h2>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>
<?php get_footer(); ?>
