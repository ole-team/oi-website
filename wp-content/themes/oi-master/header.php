<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Condensed:wght@200;600&family=IBM+Plex+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;1,200;1,400;1,600&family=Work+Sans:wght@700&display=swap" rel="stylesheet">
	 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<head>
	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper container">
			<?php if(!is_page('contacto')){ ?> 
			<?php oi_the_custom_logo()?>
			<?php }else{ ?>
				<a href="http://localhost/ole/oi-site/oi-website/" class="brand-logo left" rel="home" aria-current="page"><img width="73" height="57" src="<?php echo get_theme_mod( 'oi_dark_version' );?>" class="custom-logo" alt="OI"></a>
				
			<?php }?>
			<?php wp_nav_menu( array( 'theme_location' => 'mainmenu','menu_class'=> 'right hide-on-med-and-down', 'container'=>false) ); ?>
			<?php wp_nav_menu( array( 'theme_location' => 'mainmenu','menu_class'=> 'sidenav', 'container'=>'div','container_id'=>'wrapper-nav-mb','menu_id'=>'nav-mobile') ); ?>
  				<a href="#" data-target="nav-mobile" class="sidenav-trigger right"><i class="material-icons">menu</i></a>
			</div>
		</nav>
	</div>
</head>

	
