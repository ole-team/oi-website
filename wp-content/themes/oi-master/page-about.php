<?php 
/* 
Template Name: About
*/?>
<?php get_header(); ?>
<section class="intro">
	<div class="container">
	<div class="content">
		<div class="left-content">
		<img src="<?php echo get_template_directory_uri();?>/assets/p1.png" class="wow fadeInDown anima-oi" data-wow-delay="3s" data-wow-offset="20">
		<img src="<?php echo get_template_directory_uri();?>/assets/p2.png" class="wow fadeInDown" data-wow-offset="20">
		<img src="<?php echo get_template_directory_uri();?>/assets/p3.png" class="wow fadeInDown" data-wow-offset="20" data-wow-delay="1s">
		<img src="<?php echo get_template_directory_uri();?>/assets/p4.png" class="wow fadeInDown" data-wow-offset="20" data-wow-delay="2s">
		</div>
		<div class="elemento e-a wow bounceInUp" data-wow-offset="10" data-wow-delay="3s">
	      <img src="<?php echo get_template_directory_uri();?>/assets/about1.png" />
	    </div>
	    <div class="elemento e-b wow bounceInUp anima-oi" data-wow-offset="20" data-wow-delay="2s">
	      <img src="<?php echo get_template_directory_uri();?>/assets/about2.png" />
	    </div>
	    <div class="elemento e-c wow bounceInUp anima-oi" data-wow-offset="10" data-wow-delay="2s">
	      <img src="<?php echo get_template_directory_uri();?>/assets/about3.png" />
	    </div>
	    <div class="elemento e-d wow bounceInUp" data-wow-offset="10" data-wow-delay="2s">
	      <img src="<?php echo get_template_directory_uri();?>/assets/about4.png" />
	    </div>
	    <div class="elemento e-e wow bounceInUp anima-oi" data-wow-offset="10" data-wow-delay="2s">
	      <img src="<?php echo get_template_directory_uri();?>/assets/about5.png" />
	    </div>
	</div>
	</div>
</section>
<section class="about bg dark">
	<div class="container">
	<div class="row">
		<div class="col m5">
			<?php 
			$about   = get_post( 26 );
			$ccabout =  apply_filters( 'the_content', $about->post_content );
			$ttabout =  apply_filters( 'title', $about->post_title );
			?>
			<h1><?php echo $ttabout; ?></h1>
		</div>
		<div class="col s3 m2 element-aa">
		</div>
		<div class="col s9 m5 txt-about">
			<?php echo $ccabout; ?>
		</div>
	</div>
	</div>
</section>
<section class="services">
	<div class="container">
		<h2>Full digital<br /><span>Package</span></h2>
		<div class="row tabs-services">
		    <div class="col s12">
		      <ul class="tabs">
		        <li class="tab col s4"><a class="active" href="#noise"><img src="<?php echo get_template_directory_uri();?>/assets/oi-voice.png" /></a></li>
		        <li class="tab col s4"><a href="#voice"><img src="<?php echo get_template_directory_uri();?>/assets/oi-noise.png" /></a></li>
		        <li class="tab col s4"><a href="#droids"><img src="<?php echo get_template_directory_uri();?>/assets/oi-droids.png" /></a></li>
		      </ul>
		    </div>
		    <div id="noise" class="col s12 service-info">
		    	<h3>ESTRATEGIA Y CONTENIDO</h3>
		    	<div class="col s6">
		    		<h4><span>01.</span><br />Digital Strategy</h4>
		    		<p>Estrategia para <strong>entender a tu audiencia</strong> y crear comunidades para alcanzar tus KPIs deseados.</p>
		    	</div>
		    	<div class="col s6">
		    		<h4><span>02.</span><br />Creative & Prod Studio</h4>
		    		<p>Una <strong>boutique creativa</strong> al servicio de tu marca: desde el guión hasta la post-producción.</p>
		    	</div>
		    </div>
		    <div id="voice" class="col s12 service-info">
		    	<h3>MEDIOS PROPIOS</h3>
		    	<div class="col s6 offset-s3">
		    		<h4><span>03.</span><br />Branded Content</h4>
		    		<p>Difusión en el grupo de publishers con mayor engagement en habla hispana a <strong>255 millones de alcance.</strong> </p>
		    	</div>
		    </div>
		    <div id="droids" class="col s12 service-info">
		    	<div class="col s6">
		    		<h3>AMPLIFICACIÓN</h3>
		    		<h4><span>04.</span><br />Media Buying</h4>
		    		<p>Tu campaña llega a cientos de websites, videojuegos,  connected TV y OTTs.<br />
		    		Social<br />
		    		Search<br />
		    		Web</p>
		    	</div>
		    	<div class="col s6">
		    		<h3>SERVICIOS</h3>
		    		<h4><span>05.</span><br />Digital Services</h4>
		    		<p>Web Design + Development<br />
					Ad ops as a service<br />
					Infraestructura<br />
					Education Platform<br />
					Livestreaming, Virtual events</p>
					<a href="https://www.revamplify.com/" target="_blank">
						<img src="<?php echo get_template_directory_uri();?>/assets/revamplify_logo.png" />
					</a>
		    	</div>
		    </div>
	  </div>
	</div>
</section>
<section class="cases-home">
	<div class="row">
		<?php
		$the_query = new WP_Query(array( 'cat' => 2,'posts_per_page' => '3') );
		if ( $the_query->have_posts() ) : ?>
		<div class="col m3 s12 offset-m1 tt-cat-cc">
			<?php $this_category = get_category(2); ?>
			<h3><?php echo $this_category->cat_name ?></h3>
			<a href="">VER MÁS +</a>
		</div>
		<div class="col m8 s12">
		<?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
			<div class="col m4 s12 term-cs">
				<?php if ( has_post_thumbnail() ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'carousel-pic-oi' );
					echo '<a href="'.get_permalink().'"><img src="'.$image[0].'" width="100%" height="100%" alt=""/></a>'; 
				}
				echo '<a href="'.get_permalink().'"><strong>'.get_the_title().'</strong></a><br />'; 
				echo '<a href="'.get_permalink().'"><span>+INFO</span></a>'; 
				?>					
			</div>
		<?php endwhile;?>
		</div>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
	</div>
</section>
<?php get_footer(); ?>