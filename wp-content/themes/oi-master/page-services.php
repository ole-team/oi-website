<?php 
/* 
Template Name: Services
*/?>
<?php get_header(); ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col m5 s12">
				<div class="infog">
				<div class="flip-card-inner">
					<div class="flip-card-front">
						<h3>We use data</h3>
						<span>+INFO</span>
					</div>
					<div class="flip-card-back">
						<h3>DATA INSIGHTS</h3>
						<p>Targeting the desired KPIs, we have a strategy team generating insights that combine the interests of the audience with the attributes of your brand.</p>
						<strong class="tg">CROWDTANGLE</strong>
						<strong class="tg">SOCIAL BAKERS</strong>
						<strong class="tg">AUDIENCE INSIGHTS</strong>
						<strong class="tg">ANALYTICS</strong>
						<strong class="tg">GOOGLE TRENDS</strong>
					</div>
        		</div></div>
			</div>
			<div class="col m7 s12 graph">
				<img src="<?php echo get_template_directory_uri();?>/assets/g1.png" class="graph wow bounceIn" data-wow-offset="20">
			</div>
		</div>
		<div class="row">
			<div class="col m7 s12 graph">
				<img src="<?php echo get_template_directory_uri();?>/assets/g2.png" class="graph wow bounceIn" data-wow-offset="20">
			</div>
			<div class="col m5 s12">
				<div class="infog">
					<div class="flip-card-inner">
		          <div class="flip-card-front">
		            <h3>To Create</h3>
		            <span>+INFO</span>
		          </div>
		          <div class="flip-card-back">
		            <h3>SHAPING IDEAS</h3>
		            <p>In alliance with the Strategy Team, we have a multidisciplinary global team of storytellers, journalists, writers and screenwriters ready to shape impactful and engaging narratives.</p>
		            <strong class="tg">CIUDAD DE MEXICO</strong>
		            <strong class="tg">SÃO PAULO</strong>
		            <strong class="tg">BUENOS AIRES</strong>
		            <strong class="tg">BOGOTÁ</strong>
		            <strong class="tg">MIAMI</strong>
		          </div>
		        </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col m5 s12">
				<div class="infog">
				<div class="flip-card-inner">
		          <div class="flip-card-front">
		            <h3>Produce</h3>
		            <span>+INFO</span>
		          </div>
		          <div class="flip-card-back">
		            <h3>PRODUCE</h3>
		            <p>We have an in-house structure and a ‘ready-to-go’ network of producing partners to quickly scale our production, under our creative supervision</p>
		            <strong class="tg">LIVE STREAMINGS </strong>
		            <strong class="tg">INTERVIEWS</strong>
		            <strong class="tg">RECIPES</strong>
		            <strong class="tg">HACKS</strong>
		            <strong class="tg">ANIMATIONS </strong>
		            <strong class="tg">SOCIAL EXPERIMENTS </strong>
		            <strong class="tg">PODCASTS </strong>
		            <strong class="tg">EVENTS </strong>
		          </div>
		        </div>
        		</div>
			</div>
			<div class="col m7 s12 graph">
				<img src="<?php echo get_template_directory_uri();?>/assets/g3.png" class="graph wow bounceIn" data-wow-offset="20">
			</div>
		</div>
		<div class="row">
			<div class="col m7 s12 graph">
				<img src="<?php echo get_template_directory_uri();?>/assets/g4	.png" class="graph wow bounceIn" data-wow-offset="20">
			</div>
			<div class="col m5 s12">
				<div class="infog">
					<div class="flip-card-inner">
		          <div class="flip-card-front">
		            <h3>Deliver</h3>
		            <span>+INFO</span>
		          </div>
		          <div class="flip-card-back">
		            <h3>CUT & PASTE</h3>
		            <p>Our team of editors, motion designers and animators are experts in digital formats that will engage your audience with compelling content.</p>
		            <strong class="tg">STORIES</strong>
		            <strong class="tg">IN-STREAM ADS</strong>
		            <strong class="tg">PRE-ROLLS</strong>
		            <strong class="tg">TIK TOKS</strong>
		            <strong class="tg">REELS</strong>
		            <strong class="tg">MOTION GRAPHICS</strong>
		            <strong class="tg">LONG-FORMATS</strong>
		            <strong class="tg">GIFS</strong>
		            <strong class="tg">CINEMAGRAPHS</strong>
		            <strong class="tg">VIDEO FEED</strong>
		            <strong class="tg">LIVES</strong>
		          </div>
		        </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col m5 s12">
				<div class="infog">
				<div class="flip-card-inner">
		          <div class="flip-card-front">
		            <h3><span>&</span>Distribute</h3>
		            <span>+INFO</span>
		          </div>
		          <div class="flip-card-back">
		            <h3>WE DELIVER</h3>
		            <p>As important as knowing how to create content is knowing how to distribute it to strategically achieve your campaign objectives</p>
		            <strong class="tg">INSTAGRAM</strong>
		            <strong class="tg">FACEBOOK</strong>
		            <strong class="tg">TWITTER</strong>
		            <strong class="tg">LINKEDIN</strong>
		            <strong class="tg">TIK TOK</strong>
		            <strong class="tg">YOUTUBE </strong>
		            <strong class="tg">PINTEREST</strong>
		            <strong class="tg">PROGRAMMATIC</strong>
		            <strong class="tg">GAMMING</strong>
		          </div>
		        </div>
        		</div>
			</div>
			<div class="col m7 s12 graph">
				<img src="<?php echo get_template_directory_uri();?>/assets/g5.png" class="graph wow bounceIn" data-wow-offset="20">
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>