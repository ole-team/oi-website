<?php get_header(); ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col m11 s12 offset-m1">
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="col m4 s12">
					<?php echo '<h1>'.get_the_title().'</h1>'; ?>
				</div>
				<div class="col m8 s12 text-page">
				<?php the_content(); ?>
				</div>
			<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>