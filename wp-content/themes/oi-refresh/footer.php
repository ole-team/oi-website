<footer>
	<div class="row ">
		<div class="col m12 s12 copyright">
			<p>© 2022 Ole Interactive. All Rights Reserved.</p>
		</div>
	</div>
</footer>
<script type="text/javascript">
(function($) {
$(document).ready(function(){
	$(window).scroll(function(){
		var scroll = $(window).scrollTop();
	  	if (scroll > 100) {
	  		$('nav').addClass('nav-light');
	  	}else{
	  		$('nav').removeClass('nav-light');
	  	}
	});
	$('.owl-carousel').owlCarousel({
        items:1,
        loop:true,
        lazyLoad:true,
        autoplay:true,
    	autoplayTimeout:5000,
    	nav: true
    })
	$('.carousel.carousel-slider').carousel({
    fullWidth: true
  });
});
$('.moveNextCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('next');
   });

   // move prev carousel
   $('.movePrevCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('prev');
   });
})(jQuery);
</script>
<?php wp_footer(); ?>
</body>
</html>