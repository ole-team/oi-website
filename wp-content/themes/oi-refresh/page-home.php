<?php 
/* 
Template Name: Home
*/?>
<?php get_header(); ?>
<section id="intro">
	<div class="container">
	<div class="row">
		<div class="col m4 s12 hide-on-small-only">
			<div class="intro-nav">
			<ul>
				<li><a class="current" href="#intro">home</a></li> 
				<li><a href="#solutions">soluciones</a></li> 
				<li><a href="#medios">nuestros medios</a></li>
				<li><a href="#cases">casos de éxito</a></li> 
				<li><a href="#contacto">contacto</a></li> 
			</ul>
			</div>
			
		</div>
		<div class="col m8 s12 vidCarousel">
			<div class="owl-carousel owl-theme">
			
			<img class="pic-temp" src="<?php echo get_template_directory_uri();?>/assets/playground-intro.gif">
			<img class="pic-temp" src="<?php echo get_template_directory_uri();?>/assets/i24-intro.gif">
			<img class="pic-temp" src="https://images.squarespace-cdn.com/content/v1/6106c74eec045f064badfebe/127ba0dd-1694-4e8d-a85e-f6ca31c43d4f/giphy-3.gif">
		
			</div>
		</div>
	</div>
	</div>
</section>
<section id="about">
	<div class="container">
	<div class="row">
		<?php 
			$about   = get_post( 26 );
			$ccabout =  apply_filters( 'the_content', $about->post_content );
			$ttabout =  apply_filters( 'title', $about->post_title );
			?>
		<div class="col m11 s12 offset-m1">
			<div class="col m5 s12 text-about">
				<h1><?php echo $ttabout; ?></h1>
				<div class="txt-about">
				<?php echo $ccabout; ?>
				</div>
			</div>
			<div class="col m7 s12 graph-about hide-on-small-only">
				<div class="bg-graph-about">
				</div>
				<img class="element-about" src="<?php echo get_template_directory_uri();?>/assets/oi_pic_about.png">
			</div>
		</div>
		<div class="col m10 s12 offset-m2">
			<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/698351565?h=8abdbfd5e8&loop=1&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
		</div>
	</div>
	</div>
</section>
<section id="solutions">
	<div class="container">
	<div class="row">
		<div class="col m11 s12 offset-m1">
			<div class="col m6 s12 intro-solutions">
				<h2>Soluciones</h2>
				<p>Desarrollamos soluciones digitales, para crear, producir y distribuir contenidos que impactan eficientemente a millones de personas, tanto en nuestros medios nativos como con las campañas que ideamos y amplificamos para nuestros clientes.</p>
			</div>
			<div class="col m6 s12 item-solutions">
				<div>
					<h3>Digital Strategy</h3>
					<p>Estrategia para <strong>entender a tu audiencia</strong> y crear comunidades para alcanzar tus KPIs deseados.</p>
				</div>
			</div>
			<div class="col m6 s12 item-solutions">
				<div><h3>Creative & Prod Studio</h3>
					<p>Una <strong>boutique creativa</strong> al servicio de tu marca: desde el guión hasta la post-producción</p>
				</div>
			</div>
			<div class="col m6 s12 item-solutions">
				<div><h3>Branded Content</h3>
				<p>Difusión en el grupo de publishers con mayor engagement en habla hispana a <strong>255 millones de alcance.</strong></p>
				</div>
			</div>
			<div class="col m6 s12 item-solutions">
				<div><h3>Media Buying</h3>
				<p>Tu campaña llega a cientos de websites, videojuegos, connected TV y OTTs.<br />
				Social<br />
				Search<br />
				Web</p>
				</div>
			</div>
			<div class="col m6 s12 item-solutions">
				<div><h3>Digital Services</h3>
				<p>Web Design + Development<br />
				Ad ops as a service<br />
				Infraestructura<br />
				Education Platform<br />
				Livestreaming, Virtual events</p>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<section id="medios">
	<div class="container">
	<div class="row">
		<div class="col m11 s12 offset-m1">
			<div class="col m3 s12">
				<?php $this_category = get_category(6); ?>
				<h2><?php echo $this_category->cat_name ?></h2>
				<div class="options-carousel-brand">
				<a href="Previo" class="movePrevCarousel middle-indicator-text waves-effect waves-light content-indicator"><</a>
				<a href="Siguiente" class="moveNextCarousel middle-indicator-text waves-effect waves-light content-indicator">></a>
				</div>
			</div>
			<div class="col m9 s12">
			<?php
			$the_brands = new WP_Query(array( 'cat' => 6,'posts_per_page' => '10') );
			if ( $the_brands->have_posts() ) : ?>
			<div class="carousel carousel-slider">
			<?php while ( $the_brands->have_posts() ) : $the_brands->the_post();
			$siteUrl = get_post_meta($post->ID,'url',true);
			?>
			<?php if ( has_post_thumbnail() ) {
				$imagebrand = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?> 
			<?php echo ' <div class="carousel-item" href=""><img src="'.$imagebrand[0].'" /><div class="wrap-brand"><div class="txt-brand"><strong>'.get_the_title().'</strong><p>'.get_the_content().'</p></div><div><a href="'.$siteUrl.'" target="_blank">IR AL SITIO WEB</a></div></div></div>' ;?>	 
			<?php } ?>
			<?php endwhile;?>
				</div>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
	</div>
</section>
<section id="cases">
	<div class="container">
	<div class="row">
		<?php
		$the_query = new WP_Query(array( 'cat' => 2,'posts_per_page' => '4') );
		if ( $the_query->have_posts() ) : ?>
		<div class="col m11 s12 offset-m1">
			<div class="cards-container">
			<div class="carde">
				<h2>Casos de éxito</h2>
			</div>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
			$views = get_post_meta( get_the_ID(),'views',true );
			$reached = get_post_meta( get_the_ID(),'reached_users',true );
			$result = get_post_meta( get_the_ID(),'result',true );
			?>
			<div class="carde">
				<div class="info-case">
				<?php if ( has_post_thumbnail() ) {
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'carousel-pic-oi' ); 
				$imagel = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'carousel-pic-oi-large' ); 
				echo '<picture><source media="(min-width:650px)" srcset="'.$imagel[0].'">
					  <source media="(min-width:465px)" srcset="'.$image[0].'">
					  <img src="'.$image[0].'" alt="'.get_the_title().'" style="width:100%;height:100%;">
					</picture>';
				}; ?>
				<div class="overlay-case">
					<?php echo '<div class="desc-case"><strong class="tt-case-d">'.get_the_title().'</strong><div class="kpi-info">'; 
					if(!empty($views)){
						echo '<span>'.$views.'</span><p>VIEWS</p>';
					}
					if(!empty($reached)){
						echo '<span>'.$reached.'</span><p>REACHED USERS</p>';
					}
					if(!empty($result)){
						echo '<span>'.$result.'</span><p><strong>ABOVE</strong> THE EXPECTED RESULT</p>';
					}
					 echo '</div></div>'; ?>
				</div>
				</div>
				<div class="tt-case">
					<?php echo '<strong class="tt-case">'.get_the_title().'</strong>'; ?>
				</div>
			</div>
			<?php endwhile;?>
			</div>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
	</div>
</section>
<section id="contacto">
	<div class="container">
	<div class="row">
		<div class="col m11 s12 offset-m1 wrap-contact">
			<div class="tt-contact hide-on-small-only">
				<h2>Contacto</h2>
			</div>
			<div class="col m3 contacto-office">
				<div class="office">
				<h2 class="hide-on-large-only">Contacto</h2>
				<strong>NUESTRAS OFICINAS</strong>
				<ul>
					<li>Miami</li>
					<li>Mexico City</li>
					<li>Sao Paulo</li>
					<li>Buenos Aires</li>
					<li>Bogotá</li>
					<li>Caracas</li>
				</ul>
				</div>
			</div>
			<div class="col m9 box-dark">
				<div class="col m8 offset-m2">
					<?php echo do_shortcode('[contact-form-7 id="79" title="Contact form 1"]'); ?>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<?php get_footer(); ?>