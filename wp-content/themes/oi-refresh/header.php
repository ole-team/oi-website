<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Condensed:wght@200;600&family=IBM+Plex+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;1,200;1,400;1,600&family=Work+Sans:wght@700&display=swap" rel="stylesheet">
	 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	 <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-VF432S3PK4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-VF432S3PK4');
</script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<head>
<div class="navbar-fixed">
	<nav>
		<div class="nav-wrapper container">
		
		<?php oir_the_custom_logo()?>
		
		</div>
	</nav>
</div>
</head>

	
