<?php
function oir_setup() {
	// Agregar al tema la funcionalidad del menu
	register_nav_menu( 'mainmenu', 'Menú principal' );
	register_nav_menu( 'footermenu', 'Menu footer' );

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
	add_image_size('carousel-pic-oi',390,390, true);
	add_image_size('carousel-pic-oi-large',600,600, true);
 	add_image_size('list-pic-oi',590,330, true);
	$defaults = array(
		'height'      => 240,
		'width'       => 70,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	);
 	add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'oir_setup' );

function oir_scripts_styles() {
	global $wp_styles;
	wp_enqueue_style( 'materialize-style', get_template_directory_uri().'/css/materialize.css');
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri().'/css/owl.carousel.min.css');
	wp_enqueue_style( 'owl-theme', get_template_directory_uri().'/css/owl.theme.default.css');
	wp_enqueue_style( 'oi-style', get_stylesheet_uri());
	wp_enqueue_script( 'materialize',get_template_directory_uri() . '/js/materialize.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'owl-script',get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '', true );
}
add_action( 'wp_enqueue_scripts', 'oir_scripts_styles' );

function oir_customizer_setting($wp_customize) {
// add a setting 
    $wp_customize->add_setting('oir_dark_version');
// Add a control to upload the hover logo
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'oir_dark_version', array(
        'label' => 'Upload OI dark version Logo',
        'section' => 'title_tagline', //this is the section where the custom-logo from WordPress is
        'settings' => 'oir_dark_version',
        'priority' => 8 // show it just below the custom-logo
    )));
}

add_action('customize_register', 'oir_customizer_setting');

function oir_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Footer Area 1', 'oi-site' ),
		'id' => 'footerarea',
		'description' => __( 'Texto del footer izquierda', 'oi-site' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Area 2', 'oi-site' ),
		'id' => 'footerareab',
		'description' => __( 'Texto del footer derecha', 'oi-site' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
}
add_action( 'widgets_init', 'oir_widgets_init' );

function oir_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}
add_filter('get_custom_logo','oir_logo_class');
function oir_logo_class($html){
	$html = str_replace('custom-logo-link', 'brand-logo center', $html);
	return $html;
}